<?php

namespace Drupal\single_field_entity_form;

class SingleFieldEntityFormProviders extends \SplObjectStorage {
  public function addProvider(SingleFieldEntityFormProviderInterface $provider): void {
    $this->attach($provider);
  }
}
