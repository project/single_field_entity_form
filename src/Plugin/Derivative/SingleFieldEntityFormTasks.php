<?php

namespace Drupal\single_field_entity_form\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\single_field_entity_form\EventSubscriber\SingleFieldEntityFormRouteSubscriber;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviderInterface;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviders;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic local tasks for single field entity forms.
 */
class SingleFieldEntityFormTasks extends DeriverBase implements ContainerDeriverInterface {

  public function __construct(
    protected string $basePluginId,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected SingleFieldEntityFormProviders $providers
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_field.manager'),
      $container->get('single_field_entity_form.providers')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->providers as $provider) {
      assert($provider instanceof SingleFieldEntityFormProviderInterface);
      foreach ($provider->getFields() as $entity_type_id => $fields) {
        $weight = 200;
        foreach ($fields as $field_name) {
          $route_name = SingleFieldEntityFormRouteSubscriber::getRouteName($entity_type_id, $provider->getOperation());
          $bundle = reset($this->entityFieldManager->getFieldMap()[$entity_type_id][$field_name]['bundles']);
          $title = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle)[$field_name]->getLabel();
          $derivative_id = sprintf('%s.%s', $entity_type_id, $provider->getOperation());
          $this->derivatives[$derivative_id] = [
            'route_name' => $route_name,
            'base_route' => "entity.$entity_type_id.canonical",
            'title' => $title,
            'weight' => $weight++,
          ];
        }
      }
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
