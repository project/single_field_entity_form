<?php

namespace Drupal\single_field_entity_form\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\single_field_entity_form\Access\SingleFieldEntityFormAccessCheck;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviderInterface;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviders;
use Symfony\Component\Routing\RouteCollection;

/**
 * Single field entity form event subscriber.
 */
class SingleFieldEntityFormRouteSubscriber extends RouteSubscriberBase {

  public function __construct(protected SingleFieldEntityFormProviders $providers) {
  }

  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->providers as $provider) {
      assert($provider instanceof SingleFieldEntityFormProviderInterface);
      foreach ($provider->getFields() as $entity_type_id => $fields) {
        $operation = $provider->getOperation();
        foreach ($fields as $field_name) {
          $route = clone $collection->get("entity.$entity_type_id.edit_form");
          // This does not have any specific meaning, it just needs to be
          // something and the operation will suffice.
          $route->setPath(sprintf('%s_%s', $route->getPath(), $operation));
          $defaults = $route->getDefaults();
          // This makes the entity form use our operation.
          $defaults['_entity_form'] = "$entity_type_id.$operation";
          $route->setDefaults($defaults);
          $requirements = $route->getRequirements();
          // This access checker will check whether the actual entity has
          // the field so there is no need to try to define a route per
          // bundle.
          $requirements[SingleFieldEntityFormAccessCheck::NAME] = "$entity_type_id.$field_name";
          // It also allows access even if only the field has update access
          // so remove the entity level access checker and fold the
          // functionality of it into our access checker instead.
          unset($requirements['_entity_access']);
          $route->setRequirements($requirements);
          $collection->add(static::getRouteName($entity_type_id, $operation), $route);
        }
      }
    }
  }

  public static function getRouteName(string $entity_type_id, string $operation): string {
    return sprintf('entity.%s.%s_form', $entity_type_id, $operation);
  }

}
