<?php

namespace Drupal\single_field_entity_form;

interface SingleFieldEntityFormProviderInterface {

  /**
   * Field information.
   *
   * @return array
   *   Associative array keyed by entity type id, the value is a list of
   *   fields. For each of these fields an entity form will be generated
   *   with that field only.
   */
  public function getFields(): array;

  public function getWidgetDefinition(string $entity_type_id, string $field_name): array;

  /**
   * @return string
   *   A unique machine name. This operation will be added to every editable
   *   entity. As ContentEntityForm::init() sets the form mode to be the same
   *   as the operation, the same named form mode will be enforced to use a single
   *   widget.
   */
  public function getOperation(): string;

}
