<?php

namespace Drupal\single_field_entity_form\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides an access checker for single field entity forms.
 */
class SingleFieldEntityFormAccessCheck implements AccessInterface {

  /**
   * This is the name of the access checker.
   *
   * In the services.yml file:
   * { name: access_check, applies_to: _single_field_entity_form_access }
   */
  const NAME = '_single_field_entity_form_access';

  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    [$entity_type_id, $field_name] = explode('.', $route->getRequirement(static::NAME));
    $parameters = $route_match->getParameters();
    if ($parameters->has($entity_type_id)) {
      $entity = $parameters->get($entity_type_id);
      if ($entity instanceof FieldableEntityInterface && $entity->hasField($field_name)) {
        // @TODO add a UI for field access.
        // Core EntityAccessControlHandler::checkFieldAccess always allows
        // so field access needs to be opt in for sites that implement it.
        // return $entity->get($field_name)->access('edit', $account, TRUE)->orIf($entity->access('update', $account, TRUE));
        return $entity->access('update', $account, TRUE);
      }
    }
    // There is no sensible way forward, hard deny.
    return AccessResult::forbidden();
  }

}
