<?php

namespace Drupal\Tests\single_field_entity_form\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Create a node and test SFEF Test Field form view.
 *
 * @group node
 */
class SfefTestFieldTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'single_field_entity_form_test',
  ];

  /**
   * A user with permission to bypass content access checks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Checks sfef test field form view.
   *
   * @dataProvider useFieldAccess
   */
  public function testSFEFTestFieldNodeEdit(bool $use_field_access) {
    $roles = ['create sfef_test content'];
    if (!$use_field_access) {
      $roles[] = 'edit own sfef_test content';
    }
    $create_user = $this->drupalCreateUser($roles);
    $this->drupalLogin($create_user);
    $this->container->get('state')->set('single_field_entity_form_test_entity_field_access', $use_field_access);

    $title_key = 'title[0][value]';
    $sfef_test_field_key = 'field_sfef_test_field[0][value]';

    // Create a node and makes sure our field does not get added to the
    // normal edit form.
    $edit = [];
    $edit[$title_key] = $this->randomString(8);
    $this->drupalGet('/node/add/sfef_test');
    $this->assertSession()->fieldNotExists($sfef_test_field_key);
    $this->submitForm($edit, 'Save');
    $nid = $this->drupalGetNodeByTitle($edit[$title_key])->id();

    // Visit the single field edit form and make sure it's single field indeed.
    $this->clickLink('SFEF Test Field');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("node/$nid/edit_sfef_test_field_only");
    foreach ($this->getSession()->getPage()->findAll('css', 'input') as $element) {
      $names[] = $element->getAttribute('name');
    }
    $this->assertEqualsCanonicalizing([
      'changed',
      'field_sfef_test_field[0][value]',
      'form_build_id',
      'form_id',
      'form_token',
      'op',
    ], $names);

    // Set the value of the field and assert the edit worked.
    $edit = [];
    $edit[$sfef_test_field_key] = $this->randomString(8);
    $this->submitForm($edit, 'Save');
    $database_value = $this->container->get('entity_type.manager')->getStorage('node')
      ->loadUnchanged($nid)->field_sfef_test_field->value;
    $this->assertSame($database_value, $edit[$sfef_test_field_key]);
  }

  public function useFieldAccess() {
    return [[TRUE], [FALSE]];
  }

}
