<?php

namespace Drupal\single_field_entity_form_test;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviderInterface;

/**
 * SFEF Test Field provider for the SFEF Test field field entity form.
 */
class SFEFTestFieldEntityFormProvider implements SingleFieldEntityFormProviderInterface {

  public function getWidgetDefinition(string $entity_type_id, string $field_name): array {
    return ['type' => 'string_textfield'];
  }

  public function getFields(): array {
    return ['node' => ['field_sfef_test_field']];
  }

  public function getOperation(): string {
    return 'sfef_test_field_only';
  }
}
