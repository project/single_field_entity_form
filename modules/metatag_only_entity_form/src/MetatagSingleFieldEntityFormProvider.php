<?php

namespace Drupal\metatag_only_entity_form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\single_field_entity_form\SingleFieldEntityFormProviderInterface;

/**
 * Metatag provider for the single field entity form.
 */
class MetatagSingleFieldEntityFormProvider implements SingleFieldEntityFormProviderInterface {

  public function __construct(protected EntityFieldManagerInterface $entityFieldManager) {
  }

  public function getWidgetDefinition(string $entity_type_id, string $field_name): array {
    return [
      'type' => 'metatag_firehose',
      'settings' => [
        'sidebar' => FALSE,
        'use_details' => FALSE,
      ],
    ];
  }

  public function getFields(): array {
    $fields = [];
    foreach ($this->entityFieldManager->getFieldMap() as $entity_type_id => $data) {
      foreach ($data as $field_name => $v) {
        if ($v['type'] === 'metatag') {
          $fields[$entity_type_id] = [$field_name];
          continue 2;
        }
      }
    }
    return $fields;
  }

  public function getOperation(): string {
    return 'metatag_only';
  }

}
